#include "Vehicle.h"

Model Vehicle::vehicleModel;

Vehicle::Vehicle()
{
	speed = 0.0f;
	targetSpeed = 60.0f;
	posX = 0.0f;
	posY = 0.0f;
	startX = 0.0f;
	acc = 0.0f;
	change = 1;
}

void Vehicle::loadModel()
{
	vehicleModel.create("data/Car/BMW_M3_GTR.obj");
}

void Vehicle::Render(Shader shader)
{
	glm::mat4 model2;
	//model2 = glm::rotate(model2, glm::radians(180.f), glm::vec3(0.0f, 1.0f, 0.0f));
	model2 = glm::translate(model2, glm::vec3(posX + startX, -1.75f, posY));
	model2 = glm::scale(model2, glm::vec3(0.001f, 0.001f, 0.001f));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
//	cout << spd << std::endl;
	vehicleModel.Draw(shader);
}

float Vehicle::getPositionX()
{
	return startX + posX;
}

float Vehicle::getPositionY()
{
	return posY;
}

void Vehicle::change_lane_right(Shader shader, float t)
{
	if (posY >= 12.0f) return;
	glm::mat4 model2;
	model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, 12.0f*t));
	posY = posY + 12.0f*t;
	if (posY <= 12.5f && posY >= 11.5f) posY = 12.0f;
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
}

void Vehicle::set_position(float x, float y)
{
	startX = x;
	posY = y;
}

void Vehicle::change_lane_left(Shader shader, float t)
{
	if (posY <= -12.0f) return;
	glm::mat4 model2;
	model2 = glm::translate(model2, glm::vec3(0.0f, 0.0f, -12.0f*t));
	posY = posY - 12.0f*t;
	if (posY >= -12.5f && posY <= -11.5f) posY = -12.0f;
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
}

void Vehicle::change_lane_center(Shader shader, float t)
{
	if (posY < 0.0f) change_lane_right(shader, t);
	else if (posY > 0.0f) change_lane_left(shader, t);
	if (posY < 0.5f && posY > -0.5f) posY = 0.0f;
}

void Vehicle::ACC_System(vector<Vehicle*> traffic, float time)
{
	static vector<float> distances;
	static int vehicleNumber = -1, vehiclePosition = 60, aux = 0;
	bool found = false;
	for (int index = 1; index < traffic.size(); index++)
	{
		aux = traffic[index]->getPositionX();
		if (traffic[index]->getPositionY() == this->getPositionY() && aux > this->getPositionX())
		{
			if(found == false )
			{
				vehicleNumber = index;
				found = true;
			}
			
			if (aux < traffic[vehicleNumber]->getPositionX())
			{
				vehicleNumber = index;
			}
		}
	}
	if (vehicleNumber == -1) found = false;
	if (found == false)
	{
		distances.clear();
		vehicleNumber = -1;
		this->set_targetSpeed(ACC_speed);
		return;
	}
			float distance = traffic[vehicleNumber]->getPositionX() - this->getPositionX();
			if (distance > 60.0f)
			{
				this->set_targetSpeed(ACC_speed);
				if (distances.size() != 0)distances.clear();
			}
			else
			{
				distances.push_back(distance);
				if (distances.size() == 2)
				{
					float vehicleSpeed = (distances[1] - distances[0]) / time + this->getSpeed();
					cout << distances[1] << " ; " << vehicleSpeed << " ; " << this->getSpeed() << " ; " <<this->getTargetSpeed() << endl;
					if (distances[1] < ACC_distance)
					{
						this->set_targetSpeed(vehicleSpeed - 5.0f,1);
					}
					else if (distances[1] < ACC_distance + 10.0f && distances[1] >= ACC_distance)
					{
					//	if (distances[1] > distances[0]) continue;
						this->set_targetSpeed(vehicleSpeed,1);
					}
					else this->set_targetSpeed(ACC_speed);
					distances.clear();
				}
			}
}

//TODO acceleratia sa se modifice in functie de diverse cazuri
void Vehicle::accelerate(float t)
{
	acc = acc  + 20.0f*t;
	speed = speed + acc * t;
	if (speed >= targetSpeed)
	{
		speed = targetSpeed;
		acc = 0.0f;
		controlAcc = true;
	}
}

//TODO acceleratia sa se modifice in functie de diverse cazuri
void Vehicle::brake(float t)
{
	if(motionPriority == 0)
		acc = acc - 5.0f*t;
	if (motionPriority == 1)
		acc = acc - 25.0f*t;
	if (motionPriority == 2)
		acc = acc - 50.0f*t;
	if (motionPriority == 3)
		acc = acc - 300.0f*t;
	speed = speed + acc*t;
	if (speed <= targetSpeed)
	{
		speed = targetSpeed;
		acc = 0.0f;
		controlAcc = true;
		motionPriority = 0;
	}
}

void Vehicle::set_targetSpeed(float tSpeed, int priority)
{
	if (speed != targetSpeed && controlAcc)
	{
		acc = 0.0f;
		controlAcc = false;
	}
	targetSpeed = tSpeed;
	motionPriority = priority;
}

float Vehicle::getSpeed()
{
	return speed;
}

float Vehicle::getTargetSpeed()
{
	return targetSpeed;
}

void Vehicle::motion(float t)
{
	if (targetSpeed > speed) accelerate(t);
	if (targetSpeed < speed) brake(t);
	posX = posX + speed * t;
}

void Vehicle::set_speed(float spd)
{
	speed = spd;
}

void Vehicle::set_ACC_speed(float spd)
{
	ACC_speed = spd;
}

void Vehicle::set_ACC_dist(float dst)
{
	ACC_distance = dst;
}
