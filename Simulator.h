#pragma once
#include "GLGraphics.h"
#include "Vehicle.h"
#include "road.h"
#include "Text.h"

struct position
{
	float pX, pY;
};

class Simulator
{
public:
	Simulator(GLFWwindow* win); //constructor
	~Simulator();	//destructor
	void Go(float deltatime, int FPS);	//functia principala

private:
	void CreateFrame();	
	void traffic_control(float deltatime);	
	void manageTraffic();

	bool checkIfSafeL(Vehicle trVeh);
	bool checkIfSafeR(Vehicle trVeh);
	bool checkIfSafeC(Vehicle trVeh);

	float checkDistanceL(Vehicle veh);	//verifica distanta fata de vehiculul de pe banda stanga
	float checkDistanceR(Vehicle veh);	//verifica distanta fata de vehiculul de pe banda dreapta
	float checkDistanceC(Vehicle veh);	//verifica distanta fata de vehiculul de pe banda centrala

	void menu();
	void manage_menu();

	int checkForInput();
private:
	GLFWwindow* window;
	GLGraphics gfx;
	Shader shader;
	Vehicle* ourVehicle;  //vehiculul principal
	Vehicle* newVeh;	  //folosit pentru adaugarea de trafic de catre user
	vector <Vehicle*> traffic;  //vector ce contine toate vehiculele de pe ecran
	float randomVar = 0.0f   //variabila random pt schimbarea vitezei
		, randVar = 1;		//variabila ce retine ultima banda a unui vehicul inainte de a
							//efectua un change lane
	float trSpeed = 0.0f;	//indicele vehiculului ce isi va schimba viteza
	road ourRoad;			//drum
	bool test = false;		//indica daca vehiculul principal trebuie sa schimbe banda
	Text screenText;		//textul de pe ecran
	int currentMenuState=0, subMenuState=0;  //retin starea meniului
	string number = "";		//retine textul scris de user
	int counter = 0;		//folosit pentru sensibilitatea meniului
	bool checkADD = false;  //pt adaugarea vehiculelor de catre user		
	float cursorX = 0.0f, cursorY = 0.0f;	//cursori pt user input
};
